#!/usr/bin/env python3

import genesys
import argparse


parser = argparse.ArgumentParser(description = 'sets up a power supply system according to a configuration file')
parser.add_argument('filename', help = 'name of configuration file')
parser.add_argument('-e', '--enable', help = 'enables all supplies without prerequisites', action = 'store_true')
#parser.add_argument('-v', '--verbose', help = 'turns on verbose mode')

args = parser.parse_args()

supplies = genesys.file_import(args.filename)

port = supplies[0].port

gen_usb = genesys.Genesys_USB(port, supplies = [i.addr for i in supplies])
for i in supplies:
	i.gen = genesys.Genesys(i.addr, gen_usb)

	i.gen.output_disable()

	if(i.volt):
		i.gen.set_voltage(i.volt)
	else:
		i.gen.set_voltage(0)
	
	if(i.amps):
		i.gen.set_current(i.amps)
	else:
		i.gen.set_current(0)
	
	if(i.OVP):
		i.gen.set_ovp(i.OVP)
	else:
		i.gen.set_ovp(i.gen.max_OVP)

	if(i.UVL):
		i.gen.set_uvl(i.UVL)
	else:
		i.gen.set_uvl(0)


	if(i.lockout):
		i.gen.set_remote_local(genesys.Genesys.local_remote.LOCAL_LOCKOUT)

	if(args.enable):
		if(i.prereq == [None]):
			i.gen.output_enable()


with open('.genesys', 'w') as f:
	f.write(args.filename + '\n')