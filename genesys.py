
from __future__ import absolute_import, division, print_function, unicode_literals

import serial
import io
import sys, glob
import time
import ast
from enum import Enum


class Genesys(object):

	#ovp_range expresses the min and max ovp settings for a given voltage supply
	ovp_range = { 6:( 0.5 , 7.5 ),  8:( 0.5 , 10.0),12.5:( 1.0 , 15.0),
				 20:( 1.0 , 24.0), 30:( 2.0 , 36.0),  40:( 2.0 , 44.0),
				 60:( 5.0 , 66.0), 80:( 5.0 , 88.0), 100:( 5.0 ,110.0),
				150:( 5.0 ,165.0),300:( 5.0 ,330.0), 600:( 5.0 ,660.0)}


	#uvl_range expresses the max undervoltage lockout setting for a given voltage supply
	uvl_range =	  {   6:  5.7,  8:  7.6, 12.5: 11.9,  20: 19.0,
					 30: 28.5, 40: 38.0,   60: 57.0,  80: 76.0,
					100: 95.0, 150:142.0, 300:285.0, 600:575.0}


	class local_remote(Enum):
		LOCAL_MODE       = 'LOC'
		REMOTE_MODE      = 'REM'
		LOCAL_LOCKOUT    = 'LLO'

	class ps_mode(Enum):
		CONSTANT_CURRENT = 'CC'
		CONSTANT_VOLTAGE = 'CV'
		OUTPUT_OFF		 = 'OFF'
		UNKNOWN          = None

	class faultmode(Enum):   
		NOFAULT          = 0b00000000
		SPARE            = 0b00000001
		AC_FAIL          = 0b00000010
		OVERTEMP         = 0b00000100
		FOLDBACK_TRIP    = 0b00001000
		OVERVOLTAGE      = 0b00010000
		SHUTOFF_REAR     = 0b00100000
		OUTPUT_OFF       = 0b01000000
		ENABLE_OPEN      = 0b10000000

	class status_condition(Enum):
		CONSTANT_VOLTAGE = 0b00000001
		CONSTANT_CURRENT = 0b00000010
		NOFAULT          = 0b00000100
		FAULT_ACTIVE     = 0b00001000
		AUTO_RESTART_ENA = 0b00010000
		FOLD_ENABLED     = 0b00100000
		SPARE            = 0b01000000
		LOCAL_MODE       = 0b10000000

	class errors(Enum):
		PV_TOO_HIGH      = 'E01'
		PV_BELOW_UVL     = 'E02'
		#                = 'E03'
		OVP_TOO_LOW      = 'E04'
		#                = 'E05'
		UVL_ABOVE_PV     = 'E06'
		ON_DURING_FAULT  = 'E07'
		ILLEGAL_CMD      = 'C01'
		MISSING_PARAM    = 'C02'
		ILLEGAL_PARAM    = 'C03'
		CHECKSUM_ERROR   = 'C04'
		STG_OUT_OF_RANGE = 'C05'

	class master_slave(Enum):
		SLAVE = 0
		MASTER_1 = 1
		MASTER_2 = 2
		MASTER_3 = 3
		MASTER_4 = 4

	def __init__(self, address, gen_usb):

		self.gen_usb = gen_usb

		self.address = address
		self.gen_usb.last_address_written = self.address
		self.identification_str = gen_usb.send_command('IDN?')

		# parse id string to get data on supply
		self.partnumber = self.identification_str.split(',')[1]
		self.manufacturer = self.identification_str.split(',')[0]

		self.max_I = float(self.partnumber.split('-')[1])
		
		try:
			self.max_V = float(self.partnumber.split('-')[0].split('GEN')[1])
		except ValueError:
			self.max_V = float(self.partnumber.split('-')[0].split('GENH')[1])

		self.max_P = self.max_I * self.max_V
		self.max_OVP = Genesys.ovp_range[self.max_V][1]
		self.min_OVP = Genesys.ovp_range[self.max_V][0]
		self.max_UVL = Genesys.uvl_range[self.max_V]

		return


	def send_command(self, command, parameter = '', response = 0):
		self.gen_usb.last_address_written = self.address
		return self.gen_usb.send_command(command, parameter, response)

	def get_remote_local(self):
		return Genesys.local_remote(self.send_command('RMT?'))

	def get_voltage_setpoint(self):
		""" Returns the voltage setting. 
			Unit: Volts
		"""
		return float(self.send_command('PV?'))

	def get_voltage_output(self):
		""" Returns the output voltage. 
			Unit: Volts
		"""
		return float(self.send_command('MV?'))

	def get_current_setpoint(self):
		""" Gets the current limit. 
			Unit: Amps
		"""
		return float(self.send_command('PC?'))

	def get_current_output(self):
		""" Gets the power supply current. 
			Unit: Amps
		"""
		return float(self.send_command('MC?'))

	def get_voltage_current_data(self):
		""" Returns a string with comma seperated values:
			Measured Voltage, Programmed Voltage, Measured Current,
			Programmed Current, OVP Setpoint, UVL Setpoint
		"""
		return self.send_command('DVC?')

	def get_power_supply_status(self):
		""" returns measured and programmed voltages and currents,
			as well as interpreted status and fault registers
		"""
		pss = self.send_command('STT?').split(',')
		out = {}

		for i in pss:
			b = i.strip().split('(')
			out[b[0]] = b[1][:-1]

		out['MV'] = float(out['MV'])
		out['PV'] = float(out['PV'])
		out['MC'] = float(out['MC'])
		out['PC'] = float(out['PC'])
			
		out['SR'] = unpack_binary(int(out['SR'],16), Genesys.status_condition)

		out['FR'] = unpack_binary(int(out['FR'],16), Genesys.faultmode, Genesys.faultmode.NOFAULT)
		
		return out

	def get_ovp(self):
		""" Gets the overvoltage protection setting. 
			Unit: Volts
		"""
		return float(self.send_command('OVP?'))

	def get_uvl(self):
		""" Returns the undervoltage lockout setting. 
			Unit: Volts
		"""
		return float(self.send_command('UVL?'))

	def get_mode(self):
		""" Returns the mode from the power supply, and stores it
			Returns an enumerated type:
			ps_mode.CC for Constant current
			ps_mode.CV for Constant voltage
			ps_mode.OFF for output disabled
			ps_mode.UNKNOWN if it's unknown
		"""
		self.mode = Genesys.ps_mode(self.send_command('MODE?'))
		return self.mode

	def get_power_on_time(self):
		""" Returns the number of minutes that the supply has been on since it was built
		"""
		#a = self.send_command('\xA6' + chr(self.address))

		self.gen_usb.write(('\xA6' + chr(self.address)).encode('utf-8'))
		a = self.gen_usb.readline().decode('utf-8')

		return int(a[:8],16)    # the time is encoded as a hex integer

	def get_fault_conditional_register(self):
		return unpack_binary(int(self.send_command('FLT?'), 16), Genesys.faultmode, Genesys.faultmode.NOFAULT)

	def clear_staus(self):
		return self.send_command('CLS')

	def reset_command(self):
		return self.send_command('RST')

	def set_remote_local(self, new_remote_local):
		return self.send_command('RMT', new_remote_local.value)


	def multidrop_installed(self):
		return self.send_command('MDAV?') == '1'

	def masterslave_setting(self):
		return Genesys.master_slave(int(self.send_command('MS?')))

	def set_voltage(self, new_v):
		""" Sets the output voltage. 
			Unit: Volts
		"""
		return self.send_command('PV', coerce(new_v, 0, 1.05 * self.max_V))
	
	def set_current(self, new_i_amps):
		""" Sets the current limit. 
			Unit: Amps
		"""    
		return self.send_command('PC', coerce(new_i_amps, 0, 1.05 * self.max_I))
	
	def set_ovp(self, new_ovp_volts):
		""" Sets the overvoltage protection setting. 
			Unit: Volts
		"""
		return self.send_command('OVP', coerce(new_ovp_volts, self.min_OVP, self.max_OVP))


	def set_uvl(self, new_uvl_volts):
		""" Sets the undervoltage lockout setting. 
			Unit: Volts
		"""
		return self.send_command('UVL', coerce(new_uvl_volts, 0, self.max_UVL))

	def set_filter_freq(self, new_freq_hz):
		""" Sets the filter frequency for voltage and current measurements
			Allowable frequencies are 18, 23, and 46 hz
			throws an exception if a different value is tried

			Unit: hertz
		"""
		allowable_freqs = [18,23,46]
		if(new_freq_hz not in allowable_freqs):
			raise ValueError('Filter Freq must be 18, 23, or 46')

		return self.send_command('FILTER', new_freq_hz)
	
	def get_filter_freq(self):
		""" Returns the current filter frequency
			unit: hertz
		"""
		return int(self.send_command('FILTER?'))
	
	def output_enable(self):
		""" Enables power supply output
		"""
		return self.send_command('OUT', 1)
	
	def output_disable(self):
		""" Disables power supply output
		"""
		return self.send_command('OUT', 0)

	def identify_supply(self, flash_count = 5, time = 0.1):
		""" Flashes REM/LOC light on the front panel of the display
		"""
		save_data = self.get_remote_local()
		for i in range(flash_count):
			self.set_remote_local(Genesys.local_remote.LOCAL_MODE)
			busy_wait(time)
			self.set_remote_local(Genesys.local_remote.REMOTE_MODE)
			busy_wait(time)
		self.set_remote_local(save_data)
		return 
		
	def fast_connection_test(self):
		""" Performs a fast connection test
			returns True if connection is good
		"""
		self.gen_usb.write(('\xAA' + chr(self.address)).encode('utf-8'))
		a = self.gen_usb.readline().strip().decode('utf-8')

		return (a == '1$31')|(a == '0$30')
	
	def fast_read_registers(self):
		""" Doesn't seem to work
		"""
		a = chr(self.address + 128) + chr(self.address + 128)
		self.gen_usb.write(a.encode('utf-8'))
		b = self.gen_usb.readline().decode('utf-8')

		return b
	

	def set_fault_enable_register(self, fault_enable_bits):
		return self.send_command('FENA', fault_enable_bits)

	def get_fault_enable_register(self):
	 	return unpack_binary(int(self.send_command('FENA?'), 16), Genesys.faultmode, Genesys.faultmode.NOFAULT)

	def get_fault_event_register(self):
	 	return unpack_binary(int(self.send_command('FEVE?'), 16), Genesys.faultmode, Genesys.faultmode.NOFAULT)

	def get_status_register(self):
	 	return unpack_binary(int(self.send_command('STAT?'), 16), Genesys.status_condition)

	def set_staus_enable_register(self, status_enable_bits):
	 	return self.send_command('SENA', status_enable_bits)

	def get_status_enable_register(self):
	 	return unpack_binary(int(self.send_command('SENA?'), 16), Genesys.status_condition)

	def get_status_event_register(self):
	 	return unpack_binary(int(self.send_command('SEVE?'), 16), Genesys.status_condition)

	def global_reset(self):
	 	return self.send_command('GRST')
	
class Genesys_USB(serial.Serial):
	########################################################################
	# Overrides initialization from pySerial to force 19.2k and 8N1.
	# Runs a routine to find all of the supplies on the bus. 
	# If the supply addresses are already known, use 'supplies = [x, y ,z]'
	#   in the function call to speed operation
	def __init__(self, *args, **kwargs):
		supplies = kwargs.pop('supplies', range(31))
		super(Genesys_USB, self).__init__(*args, **kwargs)			# super() needs arguments to make this work in python 2
		self.bytesize = 8
		self.parity = 'N'
		self.stopbits = 1
		self.timeout = 5
		self.baudrate = 19200
		self.__last_address_written = None
		self.supplies = self.find_supplies(supplies)
		return

	########################################################################
	# Override readline() from pySerial to change eol character
	# from \n to \r. With this modifiction not every use of readline() 
	# waits for a timeout.
	def readline(self):
		eol = b'\r'
		leneol = len(eol)
		line = bytearray()
		while True:
			c = super(Genesys_USB, self).read(1)
			if c:
				line += c
				if line[-leneol:] == eol:
					break
			else:
				break
		return bytes(line)


	########################################################################
	# Override write() from pySerial to keep track of the last "ADR" command 
	# sent. That's stored as a property - self.last_address_written
	def write(self, data):
		if(data[:3] == b'ADR'):
			self.__last_address_written = int(data.decode('utf-8').strip().split(' ')[1])
		return super(Genesys_USB, self).write(data)

	@property
	def last_address_written(self):
		return self.__last_address_written

	@last_address_written.setter
	def last_address_written(self, value):
		if(value != self.__last_address_written):
			self.send_command('ADR', value)
		return 


	########################################################################
	# Adds a function to query the bus to determine all of the supply addresses
	def find_supplies(self, addresses = range(31)):
		timeout = self.timeout
		self.timeout = 0.5

		out = []
		for i in addresses:
			a = self.send_command('ADR', i)
			if(a[:2] == 'OK'):
				out += [i]
		
		self.timeout = timeout
		return out

	def send_command(self, command, parameter = '', response = 0):
		output = ('{} {}\n\r'.format(command, parameter).encode('utf-8'))
		self.write(output)
		self.flush()
		ret =  self.readline().strip().decode('utf-8')
		#print(output)
		#print(ret)
		return ret

def coerce(val, min_val, max_val):
	""" Forces a value to lie within a given min and max.
		returns the coerced value
	"""
	if(val < min_val):
		return min_val
	if(val > max_val):
		return max_val
	return val

def unpack_binary(val, enumerated_masks, default = None):
	out = []
	for i in enumerated_masks:
		if(i.value & val):
			out += [i.name]
	if(len(out) == 0):
		out += [default]
	return out
		
def busy_wait(dt):   
    current_time = time.time()
    while (time.time() < current_time+dt):
        pass

########################################################################
# Function to find all available serial ports on the system
# Source: https://github.com/mwaylabs/fruitymesh/blob/master/util/term/startTerms.py
def serialPorts():
    """ Lists serial port names
        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result

########################################################################
# Function to help import data from a configuration file
#
def file_import(filename):

	class supply(object):
		def __init__(self, data):
			self.__dict__ = data

	try:
		configfile = open(filename, 'rt')
		filedata = ast.literal_eval(configfile.read())
	except FileNotFoundError:
		print('File not found')
		exit()
	except SyntaxError:
		print('File formatted incorrectly')
		exit()

	try:
		supplies = [supply(i) for i in filedata]
	except TypeError:
		print('File formatted incorrectly')
		exit()

	try:
		port = supplies[0].port
		for i in supplies[1:]:
			assert port == i.port
	except IndexError:
		pass
	except AssertionError:
		print('Warning: All supplies must exist on the same serial port. Using port ', port)

	names = [i.name for i in supplies] + [None]
	for i in supplies:
		if(i.prereq):
			try:
				for j in i.prereq:
					if(j not in names):
						print('Warning: supply %s has non-existant prerequisite %s'%(i.name, j))
				for j in i.parallel:	
					if(j not in names):
						print('Warning: supply %s has non-existant parallel supply %s'%(i.name, j))
				for j in i.series:	
					if(j not in names):
						print('Warning: supply %s has non-existant series supply %s'%(i.name, j))
			except IndexError:
				print('Warning: supply %s has badly formatted file.'%i.name)


	configfile.close()


	return supplies