#!/usr/bin/env python3

import genesys
import time
import csv
import argparse


def get_gen_data(supplies, datalist):
	cmd_map = {	'MV?':genesys.Genesys.get_voltage_output,
				'PV?':genesys.Genesys.get_voltage_setpoint,
				'MC?':genesys.Genesys.get_current_output,
				'PC?':genesys.Genesys.get_current_setpoint,
				'OVP?':genesys.Genesys.get_ovp,
				'UVL?':genesys.Genesys.get_uvl,
				'STAT?':genesys.Genesys.get_status_register,
				'POT?':genesys.Genesys.get_power_on_time}

	out = []

	for i in supplies:
		for j in datalist:
			try:
				out += [cmd_map[j](i)]
			except ValueError:
				print('Bad serial data')
				out += [None]

	return out

def get_header(supplies, datalist):
	nm_map = {	'MV?':'Voltage output',
				'PV?':'Voltage setpoint',
				'MC?':'Current output',
				'PC?':'Current setpoint',
				'OVP?':'OVP setting',
				'UVL?':'UVL setting',
				'STAT?':'Status register',
				'POT?':'Power on time'}


	out = []

	for i in supplies:
		for j in datalist:
			out += ['%s: %s'%(i.name,nm_map[j])]

	return out



parser = argparse.ArgumentParser(description = 'Program to store data from Genesys Power Supplies over a USB or Serial Port.')
parser.add_argument('port', help = 'serial port to use')
parser.add_argument('address', help = 'genesys addresseses to query')
parser.add_argument('-n','--name', help = 'friendly name of supplies. Add them in order of the addresses', action = 'append')
parser.add_argument('-t','--timeout', help = 'how long in seconds to run, default is 1 hour, -1 will run forever', type = int, default = 3600)
parser.add_argument('-p','--period', help = 'how often in whole seconds to query the supplies, default is 1', type = int, default = 1)
parser.add_argument('-f','--filename', help = 'name of the file to store the data, default is gen-data-XXXX, where XXXX is a string for the date and time')
parser.add_argument('-F','--filesuffix', help = 'used with -f to add a time and date suffix to a custom filename', action = 'store_true')
parser.add_argument('-v','--voltage-output', help = 'adds output voltage to data stored', dest = 'data', action = 'append_const', const = 'MV?')
parser.add_argument('-V','--voltage-setpoint', help = 'adds voltage setpoint to data stored', dest = 'data', action = 'append_const', const = 'PV?')
parser.add_argument('-i','--current-output', help = 'adds output current to data stored', dest = 'data', action = 'append_const', const = 'MC?')
parser.add_argument('-I','--current-limit', help = 'adds current limit to data stored', dest = 'data', action = 'append_const', const = 'PC?')
parser.add_argument('-o','--overvoltage-limit', help = 'adds OVL to data stored', dest = 'data', action = 'append_const', const = 'OVP?')
parser.add_argument('-u','--undervoltage-lockout', help = 'adds UVL value to data stored', dest = 'data', action = 'append_const', const = 'UVL?')
parser.add_argument('-s','--status-register', help = 'adds status registers to data stored', dest = 'data', action = 'append_const', const = 'STAT?')
parser.add_argument('-q','--power-on-time', help = 'adds power on time to data stored', dest = 'data', action = 'append_const', const = 'POT?')

args = parser.parse_args()

# finish parsing the addresses
args.address = [int(i) for i in args.address.strip('[](){}').split(',')]


# create the serial port and power supply objects
try:
	gen_usb = genesys.Genesys_USB(args.port, supplies = args.address)
	gen = [genesys.Genesys(i, gen_usb) for i in args.address]
except:
	print("Error finding supplies")
	exit() 


for i, supply in enumerate(gen):
	supply.name = 'Supply %i'%i

if(args.name):
	try:
		for i, supply in enumerate(gen):
			supply.name = args.name[i]
	except IndexError:
		print('Warning - more supplies than names')

# open the file
filetime = time.strftime("%Y-%m-%d-%a-%H%M")
if(not args.filename):
	args.filename = 'gen-data-' + filetime
elif args.filesuffix:
	args.filename = args.filename + filetime
with open(args.filename + '.csv', 'w', newline = '') as f:
	filewriter = csv.writer(f, delimiter = ',')
	print('Writing to file %s.csv'%args.filename)

	filewriter.writerow(['Time offset'] + get_header(gen, args.data))

	# loop
	start = time.time()
	t = time.time()
	idx = 0
	while(args.timeout == -1 or t-args.timeout < start):
		try:
			t = time.time()
			a = get_gen_data(gen, args.data)
			filewriter.writerow([t - start] + a )



			idx += 1
			try:
				time.sleep(start + (args.period * idx) - time.time())
			except ValueError:
				print('Overrunning period')
				while(start + (args.period * idx) < time.time()):
					idx += 1
				time.sleep(start + (args.period * idx) - time.time())
		except KeyboardInterrupt:
			break

	print('\nFile %s.csv closed after %i seconds'%(args.filename, idx*args.period))
			