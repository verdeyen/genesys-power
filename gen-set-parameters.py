#!/usr/bin/env python3

import genesys
import time
import csv
import argparse

parser = argparse.ArgumentParser(description = 'Program to manipulate parameters on genesys power supplies over a serial or USB port.')
parser.add_argument('-n', '--name', help = 'power supply name')
parser.add_argument('-p', '--port', help = 'serial port to use')
parser.add_argument('-a', '--address', help = 'genesys address to command', type = int)
parser.add_argument('-V', '--voltage-setpoint', help = 'sets output voltage', type = float)
parser.add_argument('-I', '--current-limit', help = 'sets current limit', type = float)
parser.add_argument('-o', '--overvoltage-limit', help = 'sets OVL', type = float)
parser.add_argument('-u', '--undervoltage-lockout', help = 'sets UVL', type = float)
parser.add_argument('--ON', help = 'enables power supply output', action = 'store_true')
parser.add_argument('--OFF', help = 'disables power supply output', action = 'store_true')
parser.add_argument('--ident', help ='causes the REM/LOC light to flash, indicating the targeted supply. This cannot be run with any other option', action = 'store_true')

args = parser.parse_args()

try:
	with open('.genesys', 'r') as f:
		filename = f.read().strip()
	supplies = genesys.file_import(filename)
except FileNotFoundError:
	print('Run "gen-config.py" first')
	exit()

print(supplies)
exit()

try:
	gen_usb = genesys.Genesys_USB(args.port, supplies = [args.address])
	gen = genesys.Genesys(args.address, gen_usb)
except:
	print("Error finding supplies")
	exit() 

if(args.ident):
	print(gen.identification_str)
	gen.identify_supply()

if(args.voltage_setpoint):
	gen.set_voltage(args.voltage_setpoint)

if(args.current_limit):
	gen.set_current(args.current_limit)

if(args.overvoltage_limit):
	gen.set_ovp(args.overvoltage_limit)

if(args.undervoltage_lockout):
	gen.set_uvl(args.undervoltage_lockout)

if(args.ON):
	gen.output_enable()

if(args.OFF):
	gen.output_disable()



