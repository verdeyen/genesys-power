#!/usr/bin/env python3

import genesys
import sys
import os
import argparse

parser = argparse.ArgumentParser(description = 'By default queries all avaialble serial ports\
 for all available power supplies. If a filename is given, it writes a template to the file for gen-config.py to use')
parser.add_argument('-p','--port', help = 'serial port to use')
parser.add_argument('-a','--address', help = 'genesys addresses to query', action = 'append', default = [] ,type = int)
parser.add_argument('-f','--filename', help = 'name of file to write. This will overwrite the file if it exists')
args = parser.parse_args()

s = genesys.serialPorts()

print(s)

#try:
#	configfile = open('.genesys', 'r')
#except FileNotFoundError:
#	configfile = None
#	pass
#
#if(not configfile or args.overwrite):
#	configfile = open('.genesys', 'w')
#	configfile.write('# SERIALPORT:ADDRESS:IDSTR:NAME\n\n')
if(args.filename):
	try:
		os.rename(args.filename, args.filename+'.bak')
		print('Renamed %s to %s'%(args.filename, args.filename + '.bak'))
	except FileNotFoundError:
		pass	
	configfile = open(args.filename, 'w')

if(args.port):
	if(args.port in s):
		s = [args.port]
	else:
		raise NameError('Specified port not found')
if(args.address == []):
	args.address = range(31)

if(args.filename):
	configfile.write("# This is an automatically generated file. Edit to customize.\n\n\n[\n")

for i in s:
	print('Searching port',i,'for Genesys power supplies')
	gen_usb = genesys.Genesys_USB(i, supplies = args.address)
	if(len(gen_usb.supplies) > 0):
		for supply in gen_usb.supplies:
			ps = genesys.Genesys(supply, gen_usb)
			mins = ps.get_power_on_time()
			print('%i: %s, power on time: %i hours'%(supply, ps.identification_str, mins/60))
			if(args.filename):
				configfile.write(" {\n  'name': 'SUPPLY%i',\n  'addr': %i,\n  'volt': 0,\n  'amps': 0,\n"%(supply, supply))
				configfile.write("  'OVP': 0,\n  'UVL': 0,\n  'prereq': [None],\n  'parallel': [None],\n")
				configfile.write("  'series': [None],\n  'lockout': False,\n  'port': '%s'\n },\n"%i)
	else:
		print('  No supplies found')
if(args.filename):
	configfile.seek(configfile.tell()-2)
	configfile.write('\n]\n')
	configfile.close()



